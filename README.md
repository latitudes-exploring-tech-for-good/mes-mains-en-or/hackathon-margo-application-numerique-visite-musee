# Mes Mains en Or
##### Défi d'imaginer une application numérique qui permette la visite d'un musée pour des enfants à besoins spécifiques

Le musée du LaM cherche à rendre accessible au public en situation de handicap les oeuvres qu’il expose.
Le musée du LaM reçoit régulièrement des groupes d’enfants en situation de handicap.
Souhaitant leur fournir un matériel adapté afin de leur permettre d’accéder plus facilement à la compréhension des oeuvres, le musée a pensé la création d’une mallette pédagogique transportable à travers le musée.
C’est dans ce contexte qu’a été développé un livre tactile, en braille et gros caractère (Jeannette se change la vie), édité par Mes Mains en or.

##### #0 | Résultats obtenus lors du hackathon
Voici les livrables obtenus pour le défi proposé ci-dessous.

Équipe 1
+ https://github.com/Betamorfosis/HackatonTechForGood

Équipe 2
+ https://gitlab.com/cyrilcarlin0/hackathon-margo-application-numerique-visite-musee
+ Présentation : https://drive.google.com/a/latitudes.cc/file/d/1T8ekfqCjhzfqAJZ-VOx0bFXYa3t03gHe/view?usp=sharing

Équipe 4 
+ https://github.com/Vadorequest/MMEO
+ Présentation : https://drive.google.com/a/latitudes.cc/file/d/1U1mo75zPpAN1FKM6Z8GCM0xXYhJGQZHF/view?usp=sharing

Équipe 5 
+ https://gitlab.com/Laffaire/hackathon-margo-application-numerique-visite-musee
+ Présentation : https://drive.google.com/a/latitudes.cc/file/d/1SVaUkzja_NbGR-kUcwAEzCi_g0vlvqlX/view?usp=sharing


##### #1 | Présentation de Mes Mains en Or
Mes Mains en or est une maison d’édition associative de loi 1901, crée en 2010 par Caroline Chabaud.
Elle est spécialisée dans l’édition de livres adaptés aux déficients visuels (images tactiles, braille et gros caractères)

##### #2 | Problématique : visite d'un musée pour des enfants à besoins spécifiques
L’ensemble du projet (mallette et application numérique) est destiné à accompagner les enfants en situation de handicap et leurs accompagnants dans leur découverte du musée.

La visite du musée s'oriente autour de parcours à thèmes constitués de plusieurs oeuvres.

Pour répondre à cette problématique, et selon les besoins exprimés par le musée du LaM, la mallette pédagogique et l’application devront être accessibles aux enfants déficients visuels, déficients intellectuels, et ayant des troubles du spectre de l’austime.

Le but étant que chacun des enfants, malgré leur handicap, ait à leur disposition un outil ludique leur permettant de découvrir et comprendre les oeuvres à leur rythme, tout en prenant en compte les spécificités de leur handicap.

##### #3 | Le défi proposé
Pour ce hackathon, nous allons nous concentrer sur la partie "Activités numériques".
Ces activités interviennent après la présentation d'une oeuvre, afin que les enfants puisse s'approprier l'oeuvre de manière ludique.

Vous devrez mettre en éveil votre créativité pour concevoir et implémenter une application numérique pour permettre aux enfants de réaliser une activité en rapport avec une oeuvre spécifique.

Vous devrez redoublez d'imagination pour permettre à des enfants déficients de pouvoir accéder a votre activité de la manière la plus ergonomique possible !

##### #4 | Livrables
Une ou plusieurs activités numériques sous forme d'application Android au format tablette.
 
##### #5 | Ressources à disposition pour résoudre le défi
Pour vous approprier le projet, vous aurez accès aux ressources suivantes :

- La présentation du projet
- La présentation de la mallette pédagogique
- Les chartes graphiques de MMEO et du LaM
- Un exemple d'oeuvre du parcours peinture
- Une aide pour la compréhension des besoins des enfants

##### #6 | Code de conduite et philosophie du hackathon
Lors de la conception du hackathon, Margo, Latitudes et makesense ont voulu prendre des partis-pris afin de rendre celui-ci particulier. 

Il s’agit d’éléments que nous souhaitons incarner collectivement avec vous tout au long des 24h :
+ La force du collectif pour faire émerger des solutions adaptées aux associations, notamment via la session de peer-learning ;
+ Une attention portée à la conception rapide, au test et à l’itération qui doivent permettre aux associations d’avoir accès aux solutions les plus abouties possibles ;
+ Le transfert de méthodes et postures, qui pourront être appliquées et transmises aux équipes par les animateurs, les mentors ET les autres équipes ;
+ L’engagement sur des solutions ouvertes, facilement utilisables par les associations soutenues, et qui vous permettront de continuer à contribuer à l’issue du hackathon si vous le souhaitez, ou de permettre à d’autres personnes de le faire.


##### #7 | Points de contact lors du hackathon
Éva. D.C. : salariée de Mes Mains en Or

Adrien B. : consultant Margo et en charge de la préparation du défi Mes Mains en Or

Augustin C. : co-fondateur de Latitudes, et en charge de la préparation du défi Mes Mains en Or



